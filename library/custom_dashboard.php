<?php
/**
 * Our custom dashboard page
 */

/** WordPress Administration Bootstrap */
require_once( ABSPATH . 'wp-load.php' );
require_once( ABSPATH . 'wp-admin/admin.php' );
require_once( ABSPATH . 'wp-admin/admin-header.php' );
?>
<div class="wrap about-wrap">

	<h1><?php _e( 'My LaunchBoom Landing Page' ); ?></h1>
	
	<div class="about-text"><?php _e( 'Welcome to your landing page. Have you setup your site yet? Follow the setup guide below or return to the academy for more detailed information.' ); ?></div>
	
	<ol>
		<li>Change your default information</li>
		<li>Customize your homepage</li>
		<li>Customize your thank you page</li>
		<li>Optional: Setup more funnels</li>
		<li>Optional: Setup a A/B test</li>
	</ol>

</div>
<?php include( ABSPATH . 'wp-admin/admin-footer.php' );