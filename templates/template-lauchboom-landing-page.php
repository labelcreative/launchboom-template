<?php 
/*
Template Name: Launchboom Landing Page
*/

	get_header(); 
?>
<?php background('background', '.top-section-wrap'); ?>
<div class="top-logo-wrap from-m-down <?php if (get_field('color_scheme') == 'Dark Theme'): echo "top-logo-wrap--dark"; endif; ?>">
	<?php 
	if (!empty(get_field('logo'))):
		acf_image('logo', 'ma top-logo');
	else:
	?>
		<h1 class="text-center h3 bold m0 <?php if (get_field('color_scheme') == 'Dark Theme'): echo "white"; endif; ?>"><?php tf('name'); ?></h1>
	<?php endif; ?>
</div>
<?php acf_image('mobile_background', 'from-m-down top-mobile-image ma'); ?>
<div class="bgi top-section-wrap top-section-wrap-full text-center <?php if (get_field('color_scheme') == 'Dark Theme'): echo "top-section-wrap--dark"; endif; ?>">	
	<div class="cf posr z1">
		<div class="cf top-section-tall <?php if (get_field('content_left_or_right') == 'Right'): echo "top-section-tall--right"; endif; ?>">
		  	<div class="cf">
				<div class="top-content-inner">
					<div class="from-m-up">
						<h1 class="text-left heavy"><?php tf('name'); ?></h1>
					</div>
					<div class="top-content text-left mb1 pb05 normal-page">
						<hr class="from-m-up">
						<?php tf('top_content'); ?>
					</div>
					<div class="cf">
						<?php echo do_shortcode(get_field('shortcode')); ?>
					</div>
					<div class="top-cta-text mt1 text-left normal-page"><?php tf('cta_text'); ?></div>
				</div>	
			</div>
		</div>
	</div>
</div>
<?php get_template_part('inc/exit-intent-modal'); ?>
<?php get_footer(); ?>