<?php 
/*
Template Name: Thank You w/Comments
*/

    get_header(); 
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=147167442426955";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="thank-you-headline">
    <h2 class="text-center"><?php tf('headline'); ?></h2>
</div>
<?php acf_image('image_mobile', 'from-m-down'); ?>
<div class="cf thank-you-wrap"> 
    <div class="thank-you-top">
        <div class="p1 thank-you-top">
            <div class="cf mw-960">
                <div class="top-content-inner top-content-inner--comments sevencol last">
                    <div class="p1 pt0 pb0">
                        <div class="normal-page"><?php tf('intro_text'); ?></div>
                    </div>
                </div>   
                <div class="top-content-image--comments fivecol first from-m-up ">
                    <?php acf_image('image'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="cf mw-960 p1 pt0 thank-you-steps">
        <div class="thank-you-step">
            <div class="normal-page"><?php tf('text_1'); ?></div>
            <div class="thank-you-like">
                <div class="fb-like" data-href="<?php tf('like_url'); ?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
            </div>
        </div>
        <div class="thank-you-step">
            <div class="normal-page"><?php tf('text_2'); ?></div>
            <div class="cf thank-you-buttons--comments">
                <a href="#" class="button thank-you-button mt05 facebook"><?php tf('fb_button_text'); ?></a>
                <a href="#" class="button thank-you-button mt05 twitter"><?php tf('tw_button_text'); ?></a>
            </div>
        </div>
        <div class="thank-you-step">
            <div class="normal-page"><?php tf('text_3'); ?></div>
            <div class="text-center p1 mw-710 m0">
                <div class="fb-comments" data-href="<?php echo get_bloginfo("url"); ?>" data-width="100%" data-numposts="10" data-order-by="reverse_time"></div>
            </div>
        </div>
    </div>
</div>
<footer class="footer p1 text-center" id="footer" role="contentinfo">
    <p class="source-org copyright ma">&copy; <?php echo date('Y'); ?> <a href="<?php bloginfo( 'url' ); ?>"><?php bloginfo( 'name' ); ?></a>.</p>
</footer>
<script>
jQuery(document).ready(function($) {
    $(function() {
        $('.facebook').on('click', function(e) {
            e.preventDefault();
            var w = 580, h = 300,
            left = (screen.width/2)-(w/2),
            top = (screen.height/2)-(h/2);
            if ((screen.width < 480) || (screen.height < 480)) {
                window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            } else {
                window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);   
            }
        });
        
        $('.twitter').on('click', function(e) {
            e.preventDefault();
            var loc = encodeURIComponent('<?php the_field('tw_share_link'); ?>'),
            title = "<?php the_field('tw_share_text'); ?>",
            w = 580, h = 300,
            left = (screen.width/2)-(w/2),
            top = (screen.height/2)-(h/2);
            
            window.open('http://twitter.com/share?text=' + title + '&url=' + loc, '', 'height=' + h + ', width=' + w + ', top='+top +', left='+ left +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        });
    });
});
</script>
<?php get_footer(); ?>