<?php

/*
Author: Launchboom
URL: http://launchboom.com/

*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/launch.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/launch.php' ); // if you remove this, launch will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
// require_once( 'library/news-post-type.php' ); 
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
require_once( 'library/admin.php' ); 
/*
4. library/acf.php
	- adding custom acf functions to make things a little easier
*/
require_once( 'library/acf.php' ); 

/*
5. Plugin includes
*/
// custom dashboard
require_once ('library/dashboard-widget.php');


/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'launch-thumb-600', 600, 400, true );


/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function launch_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'launchtheme' ),
		'description' => __( 'The first (primary) sidebar.', 'launchtheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
}

// includes the path to favicon
function add_favicon() {
  	$favicon_url = get_stylesheet_directory_uri() . '/library/images/favicons/favicon.ico';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}
  
//  runs when you're on the login page and admin pages  
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

// remove unneeded menu items
function emersonthis_custom_menu_page_removing() {
	global $current_user;
	if ($current_user->user_login!='launchboom_admin'){
		// remove_menu_page( 'jetpack' );                    //Jetpack* 
		remove_menu_page( 'edit.php' );                   //Posts
		remove_menu_page( 'edit-comments.php' );          //Comments
		remove_menu_page( 'themes.php' );                 //Appearance
		remove_menu_page( 'plugins.php' );                //Plugins
		remove_menu_page( 'users.php' );                  //Users
		remove_menu_page( 'tools.php' );                  //Tools
	}
  // remove_menu_page( 'options-general.php' );        //Settings
}
add_action( 'admin_menu', 'emersonthis_custom_menu_page_removing' );

// hide acf
function remove_acf_menu(){
  global $current_user;
  if ($current_user->user_login!='launchboom_admin'){
    remove_menu_page( 'edit.php?post_type=acf-field-group' );
  }
}
add_action( 'admin_menu', 'remove_acf_menu', 100 );


 // remove customizer
 # Remove customizer options.
function remove_customizer_options( $wp_customize ) {
    // $wp_customize->remove_section( 'static_front_page' );
    // $wp_customize->remove_section( 'title_tagline' );
    global $current_user;
  	if ($current_user->user_login!='launchboom_admin'){
	    $wp_customize->remove_section( 'colors' );
	    $wp_customize->remove_section( 'header_image' );
	    $wp_customize->remove_section( 'background_image' );
	    $wp_customize->remove_section( 'nav' );
	    // $wp_customize->remove_section( 'themes' );
	    // $wp_customize->remove_section( 'featured_content' );
	    $wp_customize->remove_panel( 'widgets' );
	}
}
add_action( 'customize_register',
            'remove_customizer_options',
            30);

// clean up the dashboard
// Remove dashboard widgets
function remove_dashboard_meta() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
}
add_action( 'admin_init', 'remove_dashboard_meta' ); 

// hide home label
function as_remove_menus () {
       global $submenu;
        unset($submenu['index.php'][0]);
}
add_action('admin_menu', 'as_remove_menus');

