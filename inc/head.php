<head>
	<meta charset="utf-8">
	
	<?php tf('page_specific_tracking_script_opening_head'); ?>
	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(''); ?></title>
	
	<?php tfo('opening_header_code'); ?>
	

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<?php get_template_part('inc/favicon'); ?>

	<script>
    WebFontConfig = {
        google: { families: [ 'Lato:400,700,900' ] } // put your google fonts in here 
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
          '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })(); 
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>
	<?php tfo('closing_header_code'); ?>

	<?php if (is_page_template( 'templates/template-lauchboom-landing-page.php' ) || is_page_template( 'templates/template-lauchboom-thank-you-comments.php' )): ?>
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		 fbq('init', '<?php tfo('facebook_tracking_pixel'); ?>'); 
		fbq('track', '<?php if (is_page_template( 'templates/template-lauchboom-landing-page.php' )): echo "PageView"; else: echo "Lead"; endif; ?>');
		</script>
		<!-- End Facebook Pixel Code -->
	<?php endif; ?>

</head>