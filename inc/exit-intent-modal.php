
<div id="ouibounce-modal" class="ouibounce-modal">
    <div class="underlay"></div>
    <div class="modal">
    	<div class="modal-inner">
	    	<?php acf_image('modal_image', 'ma'); ?>
	        <div class="cf p1 top-form-wrap">
				<?php echo do_shortcode(get_field('shortcode')); ?>
			</div>
			<p class="text-center modal-headline ma p1 "><?php tf('modal_headline'); ?></p>
	        <div class="modal-dismiss popup-modal-dismiss tdu italic"><?php tf('dismiss_text'); ?></div>
    	</div>
    </div>
</div>
<script>
jQuery(document).ready(function($) {
	// ouibounce - the plugin is in the head
	var _ouibounce = ouibounce(document.getElementById('ouibounce-modal'), {
		aggressive: true,
		timer: 0,
	});
	$('#ouibounce-modal .popup-modal-dismiss').on('click', function() {
        $('#ouibounce-modal').hide();
    });
    $('#ouibounce-modal .modal').on('click', function(e) {
        e.stopPropagation();
    });
});
</script>